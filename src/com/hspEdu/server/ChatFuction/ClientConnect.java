package com.hspEdu.server.ChatFuction;

import com.hspEdu.server.mangeUser.OnlineUserTotal;
import com.hspEdu.qqcommon.Message;
import com.hspEdu.qqcommon.MessageType;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Author: 杨兆俊
 * Time: 2021-10-25 20:08
 **/
public class ClientConnect extends Thread{
    private String UserName;
    private Socket clientSocket;

    public ClientConnect(String userName, Socket clientSocket) {
        UserName = userName;
        this.clientSocket = clientSocket;
    }

    public String getUserName() {
        return UserName;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    @Override
    public void run() {
        try {
            while(true){
                ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
                Message mes = (Message) ois.readObject();
                //ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
                if (mes.getMesType().equals(MessageType.MESSAGE_COMM_MES)){
                    ClientConnect getConnector = OnlineUserTotal.getConnectThread(mes.getGetter());
                    ObjectOutputStream oos = new ObjectOutputStream(getConnector.getClientSocket().getOutputStream());
                    oos.writeObject(mes);
                    oos.flush();
                }else if (mes.getMesType().equals(MessageType.MESSAGE_TO_ALL_MES)){
                    String allOnlineUser = OnlineUserTotal.getAllOnlineUser();
                    String[] allUsers = allOnlineUser.split(" ");
                    for (int i = 0; i < allUsers.length; i++) {
                        if (!(allUsers[i].equals(mes.getSender()))){
                            ObjectOutputStream allOs = new ObjectOutputStream(OnlineUserTotal.getConnectThread(allUsers[i]).getClientSocket().getOutputStream());
                            mes.setGetter(allUsers[i]);
                            allOs.writeObject(mes);
                            allOs.flush();
                        }
                    }
                }else if (mes.getMesType().equals(MessageType.MESSAGE_GET_ONLINE_FRIEND)){
                    Message message = new Message();
                    message.setGetter(mes.getSender());
                    message.setMesType(MessageType.MESSAGE_RET_ONLINE_FRIEND);
                    String allOnlineUser = OnlineUserTotal.getAllOnlineUser();
                    message.setContent(allOnlineUser);
                    ClientConnect getConnector = OnlineUserTotal.getConnectThread(mes.getSender());
                    ObjectOutputStream oos2 = new ObjectOutputStream(getConnector.getClientSocket().getOutputStream());
                    oos2.writeObject(message);
                    oos2.flush();
                }else if (mes.getMesType().equals(MessageType.MESSAGE_CLIENT_EXIT)){
                    clientSocket.close();
                    OnlineUserTotal.removeConnectUser(mes.getSender());
                    System.out.println(mes.getSender()+"退出系统...");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
