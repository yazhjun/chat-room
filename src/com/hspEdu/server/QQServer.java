package com.hspEdu.server;

import com.hspEdu.server.ChatFuction.ClientConnect;
import com.hspEdu.server.mangeUser.OnlineUserTotal;
import com.hspEdu.qqcommon.MessageType;
import com.hspEdu.qqcommon.User;
import com.hspEdu.qqcommon.Message;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Author: 杨兆俊
 * Time: 2021-10-25 19:45
 **/
public class QQServer {
    private ServerSocket serverSocket=null;
    private static ConcurrentHashMap<String, User> chm=new ConcurrentHashMap<>();
    static{
        chm.put("孙悟空",new User("孙悟空","123456"));
        chm.put("杨兆俊",new User("杨兆俊","123456"));
        chm.put("猪八戒",new User("猪八戒","123456"));
        chm.put("唐三藏",new User("唐三藏","123456"));
        chm.put("沙和尚",new User("沙和尚","123456"));
    }

    public QQServer(){
        try {
            System.out.println("服务器在端口9999监听...");
            serverSocket = new ServerSocket(9999);
            while (true){
                Socket s = serverSocket.accept();
                ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
                ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
                User o =(User) ois.readObject();
                Message message = new Message();
                if (checkUser(o.getUserId(),o)){
                    message.setMesType(MessageType.MESSAGE_LOGIN_SUCCEED);
                    oos.writeObject(message);
                    oos.flush();
                    System.out.println("用户"+o.getUserId()+"成功登陆...");
                    ClientConnect clientConnect = new ClientConnect(o.getUserId(), s);
                    OnlineUserTotal.addConcurrentHashMap(o.getUserId(),clientConnect);
                    clientConnect.start();
                }else{
                    message.setMesType(MessageType.MESSAGE_LOGIN_FAIL);
                    oos.writeObject(message);
                    oos.flush();
                    ois.close();
                    oos.close();
                    s.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkUser(String UserId,User user){
        User user1 = chm.get(UserId);
        if (user1==null){
            return false;
        }
        if (!user1.getPasswd().equals(user.getPasswd())){
            return false;
        }
        return true;
    }
}
