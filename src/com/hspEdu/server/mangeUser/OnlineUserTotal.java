package com.hspEdu.server.mangeUser;

import com.hspEdu.server.ChatFuction.ClientConnect;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Author: 杨兆俊
 * Time: 2021-10-25 21:18
 **/
public class OnlineUserTotal {
    private static ConcurrentHashMap<String, ClientConnect> hm=new ConcurrentHashMap<>();
    public static void addConcurrentHashMap(String id,ClientConnect onUser){
        hm.put(id,onUser);
    }
    public static ClientConnect getConnectThread(String id){
        return hm.get(id);
    }
    public static String getAllOnlineUser(){
        String s="";
        Iterator<String> iterator = hm.keySet().iterator();
        while (iterator.hasNext()) {
            s+= iterator.next().toString()+" ";
        }
        return s;
    }
    public static void removeConnectUser(String id){
        hm.remove(id);
    }
}
